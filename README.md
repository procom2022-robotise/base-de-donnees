# Introduction

Le projet Robotise est découpé en trois parties, une application Android, une base de données et du code Python.

Le code Python réalise toutes les interactions entre la base de données et l'application Android, mais il sert aussi au pilotage des différents composants de la machine.

Ce dépôt contient uniquement la partie SQL.

# Organisation du dépot

## Introduction

Ce dépôt est organisé de la manière suivante :

1. `Un dossier contenant les fichiers d'architecture de la base de données`

2. `Un dossier contenant le fichier pour initialiser la base de données pour la mise en production`

3. `Un dossier contenant le fichier pour initialiser la base de données afin de réaliser la démonstration lors du forum des projets PROCOM 2023`

## Tree du projet

```
base-de-donnees
├── README.md
├── architecture
│   ├── V1.pdf
│   └── V2.pdf
├── demonstration
│   └── bdd.sql
└── production
    └── bdd.sql
```

## Détail de chaque dossier

Le dossier `architecture` contient les différents schémas d'architecture.

Le dossier `demonstration` contient le fichier pour initialiser la base de données afin de réaliser la démonstration lors du forum des projets PROCOM 2023.

Le dossier `production` contient le fichier pour initialiser la base de données pour la mise en production.