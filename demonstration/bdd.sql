DROP DATABASE IF EXISTS robotise;

CREATE DATABASE robotise;

DROP USER IF EXISTS 'robotise'@'localhost';

CREATE USER 'robotise'@'localhost' IDENTIFIED BY 'a693bc7501ca6755ce67329132f581ca';

GRANT ALL PRIVILEGES ON robotise.* TO 'robotise'@'localhost';

FLUSH PRIVILEGES;

CREATE TABLE robotise.machine (
	id_bec INT NOT NULL PRIMARY KEY,
	quantite INT
);

CREATE TABLE robotise.ingredient (
	id_ingredient INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	id_bec INT REFERENCES machine(id_bec),
	nom_ingredient VARCHAR (50) NOT NULL
);

CREATE TABLE robotise.cocktail (
	id_cocktail INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom_cocktail VARCHAR (50) NOT NULL
);

CREATE TABLE robotise.composition (
	id_cocktail INT REFERENCES cocktail(id_cocktail) ON DELETE CASCADE,
	id_ingredient INT REFERENCES ingredient(id_ingredient) ON DELETE CASCADE,
	quantite INT
);

INSERT INTO robotise.cocktail (nom_cocktail) VALUES ('rouge');
INSERT INTO robotise.cocktail (nom_cocktail) VALUES ('bleu');
INSERT INTO robotise.cocktail (nom_cocktail) VALUES ('jaune');
INSERT INTO robotise.cocktail (nom_cocktail) VALUES ('vert');
INSERT INTO robotise.cocktail (nom_cocktail) VALUES ('violet');
INSERT INTO robotise.cocktail (nom_cocktail) VALUES ('orange');
INSERT INTO robotise.machine(id_bec, quantite) VALUES (0, NULL);
INSERT INTO robotise.machine(id_bec, quantite) VALUES (1, NULL);
INSERT INTO robotise.machine(id_bec, quantite) VALUES (2, NULL);
INSERT INTO robotise.machine(id_bec, quantite) VALUES (3, NULL);
INSERT INTO robotise.machine(id_bec, quantite) VALUES (4, NULL);
INSERT INTO robotise.machine(id_bec, quantite) VALUES (5, NULL);
INSERT INTO robotise.ingredient (id_bec, nom_ingredient) VALUES (1, 'rouge');
INSERT INTO robotise.ingredient (id_bec, nom_ingredient) VALUES (2, 'jaune');
INSERT INTO robotise.ingredient (id_bec, nom_ingredient) VALUES (3, 'bleu');
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (1, 1, 10);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (2, 3, 10);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (3, 2, 10);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (4, 2, 5);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (4, 3, 3);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (5, 1, 5);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (5, 3, 3);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (6, 1, 5);
INSERT INTO robotise.composition (id_cocktail, id_ingredient, quantite) VALUES (6, 2, 5);
